
#include <gtest/gtest.h>
#include <entt/entity/registry.hpp>
#include <ext/entity/range/view.hpp>

TEST(entity_range_View, SingleComponent) {

    entt::DefaultRegistry registry;

    auto e0 = registry.create();
    auto e1 = registry.create();

    registry.assign<int>(e1);
    registry.assign<char>(e1);

    auto e2 = registry.create();

    registry.assign<int>(e2);

    for (int i = 1; i < 4; ++i) {
        auto e = registry.create();
        registry.assign<int>(e, 0 + i);
    }
    auto e6 = registry.create();
    
    registry.assign<int>(e6, 4);

    for (int i = 0; i < 10; ++i) {
        auto e = registry.create();
        
        registry.assign<int>(e, 100 + i);
    }

    auto view = registry.view<int>();
    auto range_ext = view.extension<entt::ext::entity::range::tag>();

    auto i = 0;

    for (auto entity : range_ext.range({ e2,e6 })) {
        auto comp = range_ext->get(entity);

        ASSERT_TRUE(comp < 5);

        ++i;
    }

    ASSERT_EQ(i, 5);
}
TEST(entity_range_View, SingleComponentEach) {
    entt::DefaultRegistry registry;

    
    for (int i = 0; i < 15; ++i) {
        auto e = registry.create();
        registry.assign<int>(e);
        registry.assign<char>(e);
    }

    auto view = registry.view<int>();
    auto range_ext = view.extension<entt::ext::entity::range::tag>();

    const auto &cview = static_cast<const decltype(view) &>(view);
    const auto &crange_ext = static_cast<const decltype(range_ext) &>(range_ext);

    std::size_t cnt = 0;

    range_ext.within({5, 12}, [&cnt](auto, int &) { ++cnt; });

    ASSERT_EQ(cnt, std::size_t{ 8 });

    crange_ext.within({ 5, 12 }, [&cnt](auto, const int &) { --cnt; });

    ASSERT_EQ(cnt, std::size_t{ 0 });
}

TEST(entity_range_PersistentView, Each) {
    entt::DefaultRegistry registry;
    registry.prepare<int, char>();

    for (int i = 0; i < 15; ++i) {
        auto e = registry.create();
        
        registry.assign<int>(e);
        registry.assign<char>(e);
    }

    auto view = registry.view<int, char>(entt::persistent_t{});
    auto range_ext = view.extension<entt::ext::entity::range::tag>();
    const auto &cview = static_cast<const decltype(view) &>(view);
    const auto &crange_ext = static_cast<const decltype(range_ext) &>(range_ext);
    std::size_t cnt = 0;

    range_ext.within({ 5, 12 }, [&cnt](auto, int &, char &) { ++cnt; });

    ASSERT_EQ(cnt, std::size_t{ 8 });

    crange_ext.within({ 5, 12 }, [&cnt](auto, const int &, const char &) { --cnt; });

    ASSERT_EQ(cnt, std::size_t{ 0 });
}

TEST(entity_range_RawView, Functionalities) {
    entt::DefaultRegistry registry;

    auto e0 = registry.create();
    auto e1 = registry.create();
    
    registry.assign<int>(e1);
    registry.assign<char>(e1);

    auto e2 = registry.create();
    
    registry.assign<char>(e2);
    for (int i = 1; i < 4; ++i) {
        auto e = registry.create();
        
        registry.assign<char>(e);
    }
    auto e6 = registry.create();
    
    registry.assign<char>(e6);

    for (int i = 0; i < 10; ++i) {
        auto e = registry.create(); 
        
        registry.assign<char>(e);
    }

    auto view = registry.view<char>(entt::raw_t{});
    auto range_ext = view.extension<entt::ext::entity::range::tag>();

    auto i = 0;

    for (auto &&component : range_ext.range({ e2,e6 })) {
        // verifies that iterators return references to components
        component = '0';
        ++i;
    }

    ASSERT_EQ(i, 5);

    for (auto &&component : range_ext.range({ e2,e6 })) {
        ASSERT_TRUE(component == '0');

        --i;
    }

    ASSERT_EQ(i, 0);
}