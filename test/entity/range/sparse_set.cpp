
#include <gtest/gtest.h>

#include <ext/entity/range/range.hpp>


TEST(entity_range_SparseSet_NoType, Functionalities) {
    entt::SparseSet<unsigned int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();

    ASSERT_NO_THROW(set.reserve(42));
    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    set.construct(42);

    for (auto i = 1; i < 10; ++i) {
        range_ext->construct(i);
    }

    range_ext.destroy({ 1,3 });
    range_ext.destroy({ 4,4 });
    range_ext.destroy({ 5,9 });

    ASSERT_EQ(set.get(42), 0u);

    ASSERT_FALSE(set.empty());
    ASSERT_EQ(set.size(), 1u);
    ASSERT_NE(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_TRUE(set.has(42));
    ASSERT_TRUE(set.fast(42));
    ASSERT_EQ(set.get(42), 0u);

    set.destroy(42);

    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    set.construct(42);

    ASSERT_EQ(set.get(42), 0u);

    set.reset();

    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    (void)entt::SparseSet<unsigned int>{std::move(set)};
    entt::SparseSet<unsigned int> other;
    other = std::move(set);
}
TEST(entity_range_SparseSet_NoType, RangeFunctionality) {
    entt::SparseSet<unsigned int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();

    set.construct(1);
    set.construct(2);
    set.construct(3);
    set.construct(4);
    set.construct(5);

    ASSERT_FALSE(range_ext.range({ 0, 1 }).valid());
    ASSERT_FALSE(range_ext.range({ 1, 0 }).valid());
    ASSERT_FALSE(range_ext.range({ 1, 6 }).valid());
    {
        auto range = range_ext.range({ 0, 0 });

        ASSERT_TRUE(range.valid());

        for (auto&& entity : range)
        {
            FAIL();
        }
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,1 });

        ASSERT_TRUE(range.valid());

        for (auto&& entity : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 1);
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,3 });

        ASSERT_TRUE(range.valid());

        for (auto&& entity : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 3);
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,5 });

        ASSERT_TRUE(range.valid());

        for (auto&& entity : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 5);
    }
    {
        auto i = 0;

        for (auto&& entity : set)
        {
            ++i;
        }

        ASSERT_EQ(i, 5);
    }
}

TEST(entity_range_SparseSet_WithType, Functionalities) {
    entt::SparseSet<unsigned int, int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();

    ASSERT_NO_THROW(set.reserve(42));
    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    set.construct(42, 3);

    for (auto i = 1; i < 10; ++i) {
        range_ext->construct(i, i);
    }

    range_ext.destroy({ 1,3 });
    range_ext.destroy({ 4,4 });
    range_ext.destroy({ 5,9 });

    ASSERT_EQ(set.get(42), 3);
    ASSERT_FALSE(set.empty());
    ASSERT_EQ(set.size(), 1u);
    ASSERT_NE(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_TRUE(set.has(42));
    ASSERT_TRUE(set.fast(42));
    ASSERT_EQ(set.get(42), 3);

    set.destroy(42);

    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    set.construct(42, 12);

    ASSERT_EQ(set.get(42), 12);

    set.reset();

    ASSERT_TRUE(set.empty());
    ASSERT_EQ(set.size(), 0u);
    ASSERT_EQ(set.begin(), set.end());
    ASSERT_FALSE(set.has(0));
    ASSERT_FALSE(set.has(42));

    (void)entt::SparseSet<unsigned int>{std::move(set)};
    entt::SparseSet<unsigned int> other;
    other = std::move(set);
}

TEST(entity_range_SparseSet_WithType, RangeFunctionality) {
    entt::SparseSet<unsigned int, int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();

    set.construct(1, 111);
    set.construct(2, 222);
    set.construct(3, 333);
    set.construct(4, 444);
    set.construct(5, 555);


    ASSERT_FALSE(range_ext.range({ 0, 1 }).valid());
    ASSERT_FALSE(range_ext.range({ 1, 0 }).valid());
    ASSERT_FALSE(range_ext.range({ 1, 6 }).valid());
    {
        auto range = range_ext.range({ 0, 0 });

        ASSERT_TRUE(range.valid());

        for (auto&& instance : range)
        {
            FAIL();
        }
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,1 });

        ASSERT_TRUE(range.valid());

        for (auto&& instance : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 1);
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,3 });

        ASSERT_TRUE(range.valid());

        for (auto&& instance : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 3);
    }
    {
        auto i = 0;
        auto range = range_ext.range({ 1,5 });

        ASSERT_TRUE(range.valid());

        for (auto&& instance : range)
        {
            ++i;
        }

        ASSERT_EQ(i, 5);
    }
    {
        auto i = 0;

        for (auto&& instance : set)
        {
            ++i;
        }

        ASSERT_EQ(i, 5);
    }
}

TEST(entity_range_SparseSet_WithType, RangeSortOrdered) {
    entt::SparseSet<unsigned int, int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();
    auto entity_range = range_ext.make_entity_range(42, 3);

    set.construct(12, 12);
    set.construct(42, 9);
    set.construct(7, 6);
    set.construct(3, 3);
    set.construct(9, 1);

    ASSERT_EQ(set.get(12), 12);
    ASSERT_EQ(set.get(42), 9);
    ASSERT_EQ(set.get(7), 6);
    ASSERT_EQ(set.get(3), 3);
    ASSERT_EQ(set.get(9), 1);

    range_ext.sort(entity_range, [](auto lhs, auto rhs) {
        return lhs < rhs;
    });

    ASSERT_EQ(entity_range.first, 42u);
    ASSERT_EQ(entity_range.second, 3u);

    ASSERT_EQ(*(set.raw() + 0u), 12);
    ASSERT_EQ(*(set.raw() + 1u), 9);
    ASSERT_EQ(*(set.raw() + 2u), 6);
    ASSERT_EQ(*(set.raw() + 3u), 3);
    ASSERT_EQ(*(set.raw() + 4u), 1);

    auto begin = set.begin();
    auto end = set.end();

    ASSERT_EQ(*(begin++), 1);
    ASSERT_EQ(*(begin++), 3);
    ASSERT_EQ(*(begin++), 6);
    ASSERT_EQ(*(begin++), 9);
    ASSERT_EQ(*(begin++), 12);
    ASSERT_EQ(begin, end);
}

TEST(entity_range_SparseSet_WithType, RangeSortReverse) {
    entt::SparseSet<unsigned int, int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();
    auto entity_range = range_ext.make_entity_range(42, 3);

    set.construct(12, 1);
    set.construct(42, 3);
    set.construct(7, 6);
    set.construct(3, 9);
    set.construct(9, 12);

    ASSERT_EQ(set.get(12), 1);
    ASSERT_EQ(set.get(42), 3);
    ASSERT_EQ(set.get(7), 6);
    ASSERT_EQ(set.get(3), 9);
    ASSERT_EQ(set.get(9), 12);

    range_ext.sort(entity_range, [](auto lhs, auto rhs) {
        return lhs < rhs;
    });

    ASSERT_EQ(entity_range.first, 3u);
    ASSERT_EQ(entity_range.second, 42u);

    ASSERT_EQ(*(set.raw() + 0u), 1);
    ASSERT_EQ(*(set.raw() + 1u), 9);
    ASSERT_EQ(*(set.raw() + 2u), 6);
    ASSERT_EQ(*(set.raw() + 3u), 3);
    ASSERT_EQ(*(set.raw() + 4u), 12);

    auto begin = set.begin();
    auto end = set.end();

    ASSERT_EQ(*(begin++), 12);
    ASSERT_EQ(*(begin++), 3);
    ASSERT_EQ(*(begin++), 6);
    ASSERT_EQ(*(begin++), 9);
    ASSERT_EQ(*(begin++), 1);
    ASSERT_EQ(begin, end);
}

TEST(entity_range_SparseSet_WithType, RangeSortUnordered) {
    entt::SparseSet<unsigned int, int> set;
    auto range_ext = set.extension<entt::ext::entity::range::tag>();
    auto entity_range = range_ext.make_entity_range(42, 3);

    set.construct(12, 6);
    set.construct(42, 3);
    set.construct(7, 1);
    set.construct(3, 9);
    set.construct(9, 12);

    ASSERT_EQ(set.get(12), 6);
    ASSERT_EQ(set.get(42), 3);
    ASSERT_EQ(set.get(7), 1);
    ASSERT_EQ(set.get(3), 9);
    ASSERT_EQ(set.get(9), 12);

    range_ext.sort(entity_range, [](auto lhs, auto rhs) {
        return lhs < rhs;
    });

    ASSERT_EQ(entity_range.first, 3u);
    ASSERT_EQ(entity_range.second, 7u);

    ASSERT_EQ(*(set.raw() + 0u), 6);
    ASSERT_EQ(*(set.raw() + 1u), 9);
    ASSERT_EQ(*(set.raw() + 2u), 3);
    ASSERT_EQ(*(set.raw() + 3u), 1);
    ASSERT_EQ(*(set.raw() + 4u), 12);

    auto begin = set.begin();
    auto end = set.end();

    ASSERT_EQ(*(begin++), 12);
    ASSERT_EQ(*(begin++), 1);
    ASSERT_EQ(*(begin++), 3);
    ASSERT_EQ(*(begin++), 9);
    ASSERT_EQ(*(begin++), 6);
    ASSERT_EQ(begin, end);
}